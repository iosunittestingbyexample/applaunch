//
//  TestingAppDelegate.swift
//  AppLaunchTests
//
//  Created by Sergio Andres Rodriguez Castillo on 11/05/23.
//

import UIKit

@objc(TestingAppDelegate)
class TestingAppDelegate: UIResponder, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        print(">>> Launching with testing AppDelegate")
        return true
    }
}
